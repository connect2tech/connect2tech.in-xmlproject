package com.c2t.sax;

import java.io.File;
import java.io.IOException;
import java.util.List;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.SAXException;
import org.xml.sax.helpers.ParserFactory;

import com.c2t.util.Util;

import jdk.internal.org.objectweb.asm.Handle;

public class XMLParserSAX2 {

	public static void main(String[] args) throws ParserConfigurationException, SAXException, IOException {
		
		try {
			
			File f = new File("D:/nchaurasia/Java-Architect/connect2tech.in-XMLProject/src/main/resources/employees_sax.xml");
			
			SAXParserFactory factory = SAXParserFactory.newInstance();
			SAXParser parser = factory.newSAXParser();
			
			MyHandler2 handler = new MyHandler2();
			parser.parse(f, handler);
		} catch (Exception e) {
			// TODO: handle exception
		}
		
	
	}

}