package com.c2t.sax.copy;

import java.io.File;
import java.io.IOException;
import java.util.List;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.SAXException;
import org.xml.sax.helpers.ParserFactory;

import com.c2t.util.Util;

public class XMLParserSAX2 {

	public static void main(String[] args) throws ParserConfigurationException, SAXException, IOException {
		SAXParserFactory parserFctory = SAXParserFactory.newInstance();
		SAXParser parser = parserFctory.newSAXParser();
		
		MyHandler2 handler = new MyHandler2();
		
		File f = new File("D:/nchaurasia/Java-Architect/connect2tech.in-XMLProject/src/main/resources/staff_dom.xml");
	
		parser.parse(f, handler);
		
	
	}

}