package com.c2t.edureka.wknd.feb19.ist20_30.session7;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

public class SaxHandler extends DefaultHandler {

	public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {

		if (qName.equals("Employees1")) {
			System.out.println("I am root");
		} else if (qName.equals("Employee")) {
			System.out.println("I am Employee");
			
			String s = attributes.getValue(0);
			
			System.out.println(s);
		} else if (qName.equals("age")) {

		}
		
		
		

	}

	public void endElement(String uri, String localName, String qName) throws SAXException {

		if (qName.equals("Employees1")) {
			System.out.println("I am root end");
		} else if (qName.equals("Employee")) {
			System.out.println("I am Employee");
		} else if (qName.equals("age")) {
			System.out.println("I am age");
		}
	}

	public void characters(char[] ch, int start, int length) throws SAXException {
		String s = new String(ch, start, length);
		System.out.println(s);
	}
}
