package com.c2t.edureka.wknd.feb19.ist20_30.session7;

import java.io.File;
import java.io.IOException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.SAXException;

public class SaxParser {
	public static void main(String[] args) throws ParserConfigurationException, SAXException, IOException {
		File f = new File(
				"D:/nchaurasia/Java-Architect/connect2tech.in-XMLProject/src/main/resources/employees_sax.xml");
		
		SAXParserFactory saxParserFactory = SAXParserFactory.newInstance();
		SAXParser saxParser = saxParserFactory.newSAXParser();

		SaxHandler saxHandler = new SaxHandler();

		saxParser.parse(f, saxHandler);

	}
}
