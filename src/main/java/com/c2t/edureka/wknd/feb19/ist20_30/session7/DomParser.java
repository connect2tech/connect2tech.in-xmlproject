package com.c2t.edureka.wknd.feb19.ist20_30.session7;

import java.io.File;
import java.io.IOException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class DomParser {
	public static void main(String[] args) throws ParserConfigurationException, SAXException, IOException {

		File f = new File("D:/nchaurasia/Java-Architect/connect2tech.in-XMLProject/src/main/resources/staff_dom.xml");
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		DocumentBuilder db = dbf.newDocumentBuilder();
		Document doc = db.parse(f);

		String name = doc.getDocumentElement().getNodeName();
		System.out.println(name);

		NodeList nodeList = doc.getElementsByTagName("staff");

		System.out.println(nodeList.getLength());

		for (int i = 0; i < nodeList.getLength(); i++) {
			Node node = nodeList.item(i);
			
			if (node.getNodeType() == Node.ELEMENT_NODE ){
				Element element = (Element)node;
				
				String id = element.getAttribute("id");
				System.out.println(id);
				
				NodeList  list = element.getElementsByTagName("firstname");
				String text = list.item(0).getTextContent();
				System.out.println(text);
				
			}
		}

	}
}
