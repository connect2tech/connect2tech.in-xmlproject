package com.c2t.appdirect.sax;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

public class MyHandlerSax extends DefaultHandler {

	public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
		// Logic of what I want to do when i encounter start Tag

		System.out.println("startElement/" + qName);
	}

	public void endElement(String uri, String localName, String qName) throws SAXException {
		System.out.println("endElement/qName=" + qName);
	}

	public void characters(char[] ch, int start, int length) throws SAXException {
		String s = new String(ch, start, length);
		System.out.println("s="+s);
	}

}
