package com.c2t.appdirect.sax;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.SAXException;

public class ReadXmlFile {
	public static void main(String[] args) throws ParserConfigurationException, SAXException, IOException {
		
		File f = new File("D:/nchaurasia/Java-Architect/connect2tech.in-XMLProject/src/main/resources/employees_sax.xml");
		InputStream is = new FileInputStream(f);
		
		SAXParserFactory factory = SAXParserFactory.newInstance();

		MyHandlerSax dh = new MyHandlerSax();
		
		SAXParser parser = factory.newSAXParser();
		parser.parse(is, dh);
	}
}
