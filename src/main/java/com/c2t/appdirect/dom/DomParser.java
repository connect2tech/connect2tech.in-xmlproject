package com.c2t.appdirect.dom;

import java.io.File;
import java.io.IOException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class DomParser {
	public static void main(String[] args) throws ParserConfigurationException, SAXException, IOException {
		File f = new File("D:/nchaurasia/Java-Architect/connect2tech.in-XMLProject/src/main/resources/staff_dom.xml");

		DocumentBuilderFactory dbfactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder doc_builder = dbfactory.newDocumentBuilder();
		Document document = doc_builder.parse(f);

		String nodeName = document.getDocumentElement().getNodeName();
		System.out.println(nodeName);

		NodeList nodeList = document.getElementsByTagName("staff");
		
		int len = nodeList.getLength();
		System.out.println("len="+len);
		
		for(int i=0;	i<len;	i++){
			Node node = nodeList.item(i);
			
			if(node.getNodeType() == Node.ELEMENT_NODE){
				Element element = (Element)node;
				String id = element.getAttribute("id");
				System.out.println(id);
				
				NodeList nl = element.getElementsByTagName("firstname");
				
				
				String text = nl.item(0).getTextContent();
				System.out.println("text="+text);
				
			}
		}
		
	}
}
