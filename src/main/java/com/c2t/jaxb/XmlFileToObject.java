package com.c2t.jaxb;

import java.io.File;
import java.io.StringReader;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

public class XmlFileToObject {
	public static void main(String[] args) {
		File f = new File(
				"D:/nchaurasia/Java-Architect/connect2tech.in-XMLProject/src/main/java/com/c2t/jaxb/employee.xml");

		JAXBContext jaxbContext;
		try {
			jaxbContext = JAXBContext.newInstance(Employee.class);

			Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();

			Employee employee = (Employee) jaxbUnmarshaller.unmarshal(f);

			System.out.println(employee);
		} catch (JAXBException e) {
			e.printStackTrace();
		}
	}
}
