package com.c2t.dom;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.DocumentBuilder;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;

import org.w3c.dom.Node;
import org.w3c.dom.Element;
import java.io.File;

public class ReadXMLFileDom_Edureka {

	public static void main(String argv[]) {

		try {
			File f = new File("D:/nchaurasia/Java-Architect/connect2tech.in-XMLProject/src/main/resources/staff_dom.xml");
			
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder db = dbFactory.newDocumentBuilder();
			Document doc = db.parse(f);
			
			String nodeName = doc.getDocumentElement().getNodeName();
			System.out.println("nodeName="+nodeName);
			
			NodeList nodeList = doc.getElementsByTagName("staff");
			System.out.println("nodeList.getLength()="+nodeList.getLength());
					
			for(int i=0;	i<3;	i++){
				
				Node node = nodeList.item(i);
				
				if(node.getNodeType() == Node.ELEMENT_NODE){
					Element element = (Element)node;
				
					String id = element.getAttribute("id");
					System.out.println("id="+id);
					
					if(id.equals("3001")){
						NodeList  nList = element.getElementsByTagName("firstname");
						String firstName = nList.item(0).getTextContent();
						System.out.println(firstName);
					
					}
				
				}
				
			}

			
		
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}