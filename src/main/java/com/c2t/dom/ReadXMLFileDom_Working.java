package com.c2t.dom;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.DocumentBuilder;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;

import org.w3c.dom.Node;
import org.w3c.dom.Element;
import java.io.File;

public class ReadXMLFileDom_Working {

	public static void main(String argv[]) {

		try {

			/*String root = Util.getRootDir();
			String file = root + "/src/main/resources/" + "staff_dom.xml";*/
			File f = new File("D:/nchaurasia/Java-Architect/connect2tech.in-XMLProject/src/main/resources/staff_dom.xml");

			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			// Parse the content of the given file as an XML document and return
			// a new DOM Document object.
			Document doc = dBuilder.parse(f);

			// optional, but recommended
			// read this -
			// http://stackoverflow.com/questions/13786607/normalization-in-dom-parsing-with-java-how-does-it-work
			// doc.getDocumentElement().normalize();

			System.out.println("Root element :" + doc.getDocumentElement().getNodeName());

			NodeList nList = doc.getElementsByTagName("staff");

			System.out.println("nList.getLength()----------------------------" + nList.getLength());

			for (int temp = 0; temp < nList.getLength(); temp++) {

				Node nNode = nList.item(temp);

				// System.out.println("/nCurrent Element :" +
				// nNode.getNodeName());

				if (nNode.getNodeType() == Node.ELEMENT_NODE) {

					Element eElement = (Element) nNode;

					String id = eElement.getAttribute("id");

					if (id.equals("3001")) {

						System.out.println("Staff id : " + id);
						
						NodeList nodeList = eElement.getElementsByTagName("firstname");
						String fname = nodeList.item(0).getTextContent();
						System.out.println(fname);
						
						NodeList lastList = eElement.getElementsByTagName("lastname");
						String last = lastList.item(0).getTextContent();
						System.out.println(last);

						/*System.out.println(
								"First Name : " + fname);
						System.out.println(
								"Last Name : " + eElement.getElementsByTagName("lastname").item(0).getTextContent());
						System.out.println(
								"Nick Name : " + eElement.getElementsByTagName("nickname").item(0).getTextContent());
						System.out.println(
								"Salary : " + eElement.getElementsByTagName("salary").item(0).getTextContent());*/

					} else {
						System.out.println("Element does not exist...");
					}

				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}