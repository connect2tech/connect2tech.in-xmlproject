package com.c2t.dom.copy;

import java.io.File;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class DomParserXml {
	public static void main(String[] args) {
		File f = new File("D:/nchaurasia/Java-Architect/connect2tech.in-XMLProject/src/main/resources/staff_dom.xml");

		try {
			//Loading document in memory.
			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			DocumentBuilder documentBuilder = dbf.newDocumentBuilder();
			Document doc = documentBuilder.parse(f);
			
			
			String nodeName = doc.getDocumentElement().getNodeName();
			System.out.println("nodeName = "+nodeName);
			
			NodeList nodeList = doc.getElementsByTagName("staff");
			
			System.out.println("nodeList.getLength()="+nodeList.getLength());
			
			for(int i=0;i<nodeList.getLength();i++){
				Node n = nodeList.item(i);
				
				if(n.getNodeType() == Node.ELEMENT_NODE){
					Element element = (Element)n;
					
					String id = element.getAttribute("id");
					
					System.out.println("id="+id);
					NodeList nl = element.getElementsByTagName("firstname");
					String text = nl.item(0).getTextContent();
					System.out.println("text="+text);
					
					
				}
				
			}
			

		} catch (Exception e) {
			// TODO: handle exception
		}

	}
}
