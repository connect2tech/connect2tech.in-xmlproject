package com.c2t.dom.copy;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.DocumentBuilder;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;

import com.c2t.util.Util;

import org.w3c.dom.Node;
import org.w3c.dom.Element;
import java.io.File;

public class ReadXMLFileExample {

	public static void main(String argv[]) {

		try {
		
			
			String root = Util.getRootDir();

			String file = root + "/src/main/resources/" + "staff_dom.xml";

			File f = new File(file);

			DocumentBuilderFactory  factory = DocumentBuilderFactory.newInstance();
			DocumentBuilder db = factory.newDocumentBuilder();
			
			Document doc = db.parse(f);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}