package com.c2t.dom.copy;

import java.io.File;
import java.io.IOException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.xml.sax.SAXException;

public class DomParser2 {
	public static void main(String[] args) throws ParserConfigurationException, SAXException, IOException {
		File f = new File("D:/nchaurasia/Java-Architect/connect2tech.in-XMLProject/src/main/resources/staff_dom.xml");
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder docBuilder = factory.newDocumentBuilder();
		docBuilder.parse(f);
	}
}
